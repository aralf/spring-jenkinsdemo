package com.conygre.training.trader.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;

import com.conygre.training.trader.model.Trade;
import com.conygre.training.trader.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import static org.hamcrest.Matchers.containsString;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TradeController.class)
public class TradeControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TradeService service;

	@Test
	public void test_invalidUrlReturnsNotFound() throws Exception {
		this.mockMvc.perform(get("/v1/tra")).andDo(print()).andExpect(status().isNotFound());
	}

	@Test
	public void test_getAllTrades_returnsFromService() throws Exception {
        Trade testTrade = new Trade();
        testTrade.setTicker("AAPL");

        Collection<Trade> trades = Arrays.asList(testTrade);

		when(service.getAllTrades()).thenReturn(trades);

		this.mockMvc.perform(get("/v1/trade")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("\"ticker\":\"AAPL\"")));
	}

	@Test
	public void test_addTrade_callsService() throws Exception {
        Trade testTrade = new Trade();
        testTrade.setTicker("AAPL");

		ObjectMapper mapper = new ObjectMapper();
    	ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(testTrade);
		
		System.out.println(requestJson);

		this.mockMvc.perform(post("/v1/trade").header("Content-Type", "application/json").content(requestJson))
					.andDo(print()).andExpect(status().isOk());

		verify(service, times(1)).addTrade(any(Trade.class));
	}
}