package com.conygre.training.trader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradeSpringBootRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradeSpringBootRestapiApplication.class, args);
	}

}
